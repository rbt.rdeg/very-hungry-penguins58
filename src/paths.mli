

module type S = sig
  val grid : Move.elt Move.grid
end

module Make (M : S) : sig
  val priority_size : int ref
  val prior_calls : int ref

  (** Ensembles de cases servant pour les configurations *)
  module HSet : Bitset.SET with type elt = Move.pos

  val grid_set : HSet.t

  val grid_of_set : HSet.t -> (Move.elt Move.grid)


  (** Liste de tous les mouvements faisables sur la configuration
      * donnée par un ensemble et une position (non présente dans
      * l'ensemble). *)
  val all_moves : Game_state.game_state -> HSet.t -> Move.pos -> Move.move list

  (** [accessible set elt] renvoie le sous-ensemble
      * correspondant à la composante connexe de [elt]. *)
  val accessible : Game_state.game_state -> HSet.t -> HSet.elt -> HSet.t

  (** [accessible set elt] renvoie le sous-ensemble
      * correspondant à la composante connexe de [elt]. *)
  val accessible_exclusive :
    Game_state.game_state -> HSet.t -> Move.pos -> Move.pos -> HSet.t option

  (** [disconnected set elt] est une heuristique qui permet
      * d'éliminer les cas où l'ensemble n'a pas été déconnecté
      * en se contentant de faire une vérification locale sur les
      * voisins de [elt]*)
  val disconnected : Game_state.game_state -> HSet.t -> HSet.elt -> bool

  (** Si [set] a été déconnecté par la suppression de [elt],
   * renvoie la liste des nouvelles composantes connexes. *)
  val split : Game_state.game_state -> HSet.t -> Move.pos -> HSet.t list

  (**Identique à split mais ne renvoie que les ccs qui
   * appartiennent au pingouin considéré*)
  val split_exclusive : Game_state.game_state -> HSet.t -> Move.pos -> HSet.t list

  (** Calcul de la solution optimale à partir d'une position
      * donnée. L'entier est simplement la longueur de la liste
      * de mouvements à effectuer. *)
  val max_path : Game_state.game_state -> Move.pos -> int * Move.pos list

end

(**Tests functions *)
(* val tests : OUnit2.test list *)
